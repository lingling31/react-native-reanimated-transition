import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
} from 'react-native';
import Tab from './src/tab';
import Accordion from './src/accordion';
import Sequence from './src/sequence';
import Ticket from './src/ticket';
import Progress from './src/progress';
import Shuffle from './src/shuffle';

const App = () => {
  return (
    <SafeAreaView style={styles.container}>
      <Tab />
      {/* <Accordion /> */}
      {/* <Sequence /> */}
      {/* <Ticket /> */}
      {/* <Progress /> */}
      {/* <Shuffle /> */}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  }
});

export default App;
