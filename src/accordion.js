import React, { useState, useRef } from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
} from 'react-native';
import { Transition, Transitioning } from "react-native-reanimated";

const transition = (
    <Transition.Sequence>
        <Transition.Out type="scale" durationMs={200} />
        <Transition.Change interpolation="easeInOut" />
        <Transition.In type="fade" durationMs={200} />
    </Transition.Sequence>
);

const Accordion = () => {
    const [showData, setShowData] = useState(false)
    const dataTab = [
        { id: 0, name: 'Alpha' },
        { id: 1, name: 'Beta' },
        { id: 2, name: 'Gamma' },
        { id: 3, name: 'Delta' },
    ]
    const ref = useRef()

    const handleShowData = () => {
        ref.current.animateNextTransition();
        setShowData(!showData)
    }

    return (
        <Transitioning.View
            ref={ref}
            transition={transition}
            style={styles.contentContainer}
        >
            <View style={styles.content}>
                <TouchableOpacity style={styles.btnShow} onPress={handleShowData}>
                    <Text style={{ color: '#fff' }}>Show</Text>
                </TouchableOpacity>
                {showData && dataTab.map(item => {
                    return (
                        <View key={item.id} style={styles.itemContainer}>
                            <Text>{item.name}</Text>
                        </View>
                    )
                })}
            </View>
        </Transitioning.View>
    );
};

const styles = StyleSheet.create({
    contentContainer: {
        flex: 1,
    },
    content: {
        width: '100%',
        padding: '5%',
        backgroundColor: 'lightgreen',
    },
    btnShow: {
        backgroundColor: 'tomato',
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 20
    },
    itemContainer: {
        height: 50,
        backgroundColor: '#fff',
        marginBottom: 20,
        alignItems: 'center',
        justifyContent: 'center'
    }
});

export default Accordion;
