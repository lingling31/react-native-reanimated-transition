import React, { useState, useRef } from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
} from 'react-native';
import { Transition, Transitioning } from "react-native-reanimated";

const transition = (
    <Transition.Together>
        <Transition.In
            type="slide-right"
            durationMs={2000}
            interpolation="easeInOut"
        />
        <Transition.Change />
        <Transition.Out type="slide-left" duration={2000} />
    </Transition.Together>
);

const Tab = () => {
    const [activeTab, setActiveTab] = useState(1)
    const tab = [1, 2]
    const ref = useRef()

    const selectTab = (tab) => {
        ref.current.animateNextTransition();
        setActiveTab(tab)
    };

    return (
        <Transitioning.View
            ref={ref}
            transition={transition}
            style={styles.tabContainer}
        >
            <View style={styles.tabActive(activeTab)} />
            {tab.map(item => {
                return (
                    <TouchableOpacity
                        key={item}
                        style={styles.tab}
                        onPress={() => selectTab(item)}
                    >
                        <Text style={styles.tabText(activeTab, item)}>Tab {item}</Text>
                    </TouchableOpacity>
                )
            })}
        </Transitioning.View>
    );
};

const styles = StyleSheet.create({
    tabContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        height: '6%',
        backgroundColor: '#eee',
    },
    tabActive: (active) => ({
        height: 50,
        width: '50%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'lightgreen',
        position: 'absolute',
        left: active == 1 ? 0 : null,
        right: active == 2 ? 0 : null,
    }),
    tab: {
        flex: 1,
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    tabText: (active, tab) => ({
        color: active == tab ? '#fff' : '#000',
        fontSize: 22
    }),
});

export default Tab;